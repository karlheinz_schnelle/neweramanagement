﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace NewEraFreezerData.Dtos
{
    public class RawIkhayaData
    {
        public Formulanames formulanames { get; set; }
        public Pools pools { get; set; }
        public Timelinetemplate timelinetemplate { get; set; }
        public bool editable { get; set; }
        public Alertgroups alertgroups { get; set; }
    }

    public class Formulanames
    {
        [JsonProperty(PropertyName = "19")]
        public string Nineteen { get; set; }

        [JsonProperty(PropertyName = "1")]
        public string One { get; set; }
    }

    public class Timeline
    {
        public bool started { get; set; }
        public bool enabled { get; set; }
    }

    public class Ioan
    {
        public string name { get; set; }
        public int id { get; set; }
        public string value { get; set; }
        public int formulaid { get; set; }
        public int alertegid { get; set; }
        public int color { get; set; }
    }

    public class Devices
    {
        public IEnumerable<Device> AllDevices => new List<Device>
        {
            FourFourSeven,
            SixTwoFive,
            SixTwoSix
        };

        [JsonProperty(PropertyName = "447")]
        public Device FourFourSeven { get; set; }

        [JsonProperty(PropertyName = "625")]
        public Device SixTwoFive { get; set; }

        [JsonProperty(PropertyName = "626")]
        public Device SixTwoSix { get; set; }
    }

    public class ThirtyThree
    {
        public string name { get; set; }
        public Timeline timeline { get; set; }
        public Devices devices { get; set; }
    }

    public class Device
    {
        public string name { get; set; }
        public List<Ioan> ioans { get; set; }
        public string timestamp { get; set; }
        public double lat { get; set; }
        public double lon { get; set; }
        public DateTime LocationDate { get; set; }
        public bool onlinestatus { get; set; }
        public int heading { get; set; }
        public int speed { get; set; }
    }

    public class Devices2
    {
        public IEnumerable<Device> AllDevices => new List<Device>
        {
            ThreeEightFive,
            FourFiveSix,
            FiveOneSeven,
            OneThreeFourNine,
            OneThreeFiveZero,
            OneEightTwoZero,
			TwoOneSevenFive
        };

        [JsonProperty(PropertyName = "385")]
        public Device ThreeEightFive { get; set; }

        [JsonProperty(PropertyName = "456")]
        public Device FourFiveSix { get; set; }

        [JsonProperty(PropertyName = "517")]
        public Device FiveOneSeven { get; set; }

        [JsonProperty(PropertyName = "1349")]
        public Device OneThreeFourNine { get; set; }

        [JsonProperty(PropertyName = "1350")]
        public Device OneThreeFiveZero { get; set; }

        [JsonProperty(PropertyName = "1820")]
        public Device OneEightTwoZero { get; set; }

        [JsonProperty(PropertyName = "2175")]
        public Device TwoOneSevenFive { get; set; }
    }

    public class OneEightTwo
    {
        public string name { get; set; }
        public Timeline timeline { get; set; }
        public Devices2 devices { get; set; }
    }

    public class Pools
    {
        [JsonProperty(PropertyName = "33")]
        public ThirtyThree ThirtyThree { get; set; }

        [JsonProperty(PropertyName = "182")]
        public OneEightTwo OneEightTwo { get; set; }
    }

    public class Event
    {
        public int start { get; set; }
        public int rawmid { get; set; }
        public int rawoff { get; set; }
        public string fmid { get; set; }
        public string foff { get; set; }
    }

    public class Timelinetemplate
    {
        public int totaldurationindays { get; set; }
        public List<Event> events { get; set; }
    }

    public class Alertgroups
    {
        [JsonProperty(PropertyName = "160")]
        public string OneSixZero { get; set; }

        [JsonProperty(PropertyName = "240")]
        public string TwoFourZero { get; set; }
    }
}
