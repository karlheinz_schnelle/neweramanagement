using System;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using NewEra.Common.Enums;
using NewEra.Data;
using NewEra.Functions.Services;
using NewEraFreezerData.Services;

namespace NewEraFreezerData
{
    public static class FetchRefrigerationInfoFunction
    {
        [FunctionName("FetchRefrigerationInfo")]
        public static void Run([TimerTrigger("0 */10 * * * *")]TimerInfo myTimer,
            TraceWriter log)
        {
            log.Info($"Fetch Refrigeration Info execution started: {DateTime.Now}");

            var freezerData = new IkhayaRefrigerationDataCollectorService().GetFreezerData();

            new RefrigerationDataRepository().SaveRefrigerationData(freezerData);

            ProcessFreezerData(freezerData);

            log.Info($"Fetch Refrigeration Info execution ended: {DateTime.Now}");
        }

        private static void ProcessFreezerData(
            NewEra.Common.Dtos.RefrigerationData refrigerationData)
        {
            foreach (var freezer in refrigerationData.Freezers)
            {
                if (!freezer.Online)
                {
                    new AlertService().RaiseAlert(AlertType.FreezerOffline, $"Freezer_{freezer.FreezerName}");
                }
                else
                {
                    new AlertService().CloseOpenAlert(AlertType.FreezerOffline, $"Freezer_{freezer.FreezerName}");
                }
            }

            foreach (var vehicle in refrigerationData.Vehicles)
            {
                if (!vehicle.Online)
                {
                    new AlertService().RaiseAlert(AlertType.VehicleOffline, $"Vehicle_{vehicle.NumberPlate}");
                }
                else
                {
                    new AlertService().CloseOpenAlert(AlertType.VehicleOffline, $"Vehicle_{vehicle.NumberPlate}");
                }
            }
        }
    }
}
