﻿using System;
using System.Net;

namespace NewEraFreezerData
{
    public class CookieAwareWebClient
        : WebClient
    {
        private CookieContainer cookieContainer { get; set; }

        public CookieAwareWebClient(
            CookieContainer container)
        {
            cookieContainer = container;
        }

        public CookieAwareWebClient()
            : this(new CookieContainer())
        { }

        public void SetCookieContainer(
            CookieContainer container)
        {
            cookieContainer = container;
        }

        protected override WebRequest GetWebRequest(
            Uri address)
        {
            var request = (HttpWebRequest)base.GetWebRequest(address);
            request.CookieContainer = cookieContainer;
            return request;
        }
    }
}
