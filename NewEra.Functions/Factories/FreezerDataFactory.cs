﻿using System.Collections.Generic;
using NewEraFreezerData.Dtos;
using NewEra.Common.Dtos;
using NewEra.Common.ValueObjects;

namespace NewEraFreezerData.Factories
{
    public static class FreezerDataFactory
    {
        public static RefrigerationData Create(
            RawIkhayaData rawData)
        {
            RefrigerationData refrigerationData = new RefrigerationData();

            refrigerationData.Freezers.AddRange(GetFreezers(rawData));
            refrigerationData.Vehicles.AddRange(GetVehicles(rawData));

            return refrigerationData;
        }

        private static List<RefrigerationData.Freezer> GetFreezers(
            RawIkhayaData rawData)
        {
            List<RefrigerationData.Freezer> freezers = new List<RefrigerationData.Freezer>();

            foreach (var device in rawData?.pools?.ThirtyThree?.devices.AllDevices)
            {
                RefrigerationData.Freezer freezer = new RefrigerationData.Freezer { Name = device.name, Online = device.onlinestatus };

                foreach (var ioan in device.ioans)
                {
                    freezer.Sensors.Add(GetSensor(ioan));
                }

                freezers.Add(freezer);
            }

            return freezers;
        }

        private static List<RefrigerationData.Vehicle> GetVehicles(
            RawIkhayaData rawData)
        {
            List<RefrigerationData.Vehicle> vehicles = new List<RefrigerationData.Vehicle>();

            foreach (var device in rawData?.pools?.OneEightTwo?.devices?.AllDevices)
            {
                if (device == null)
                {
                    continue;
                }

                RefrigerationData.Vehicle vehicle = new RefrigerationData.Vehicle { Name = device.name, Online = device.onlinestatus };

                foreach (var ioan in device.ioans)
                {
                    vehicle.Sensors.Add(GetSensor(ioan));
                }

                vehicles.Add(vehicle);
            }

            return vehicles;
        }

        private static RefrigerationData.ISensor GetSensor(
            Ioan ioan)
        {
            RefrigerationData.ISensor sensor;

            if (ioan.name == "Power")
            {
                sensor = new RefrigerationData.PowerSensor
                {
                    Id = ioan.id,
                    Name = ioan.name,
                    IsOn = ioan.value == "On",
                    FormulaId = ioan.formulaid,
                    AlertId = ioan.alertegid,
                    ColourId = ioan.color
                };
            }
            else if (ioan.name.StartsWith("Input") || ioan.name.StartsWith("Output"))
            {
                sensor = new RefrigerationData.IOSensor
                {
                    Id = ioan.id,
                    Name = ioan.name,
                    IsOn = ioan.value == "On",
                    FormulaId = ioan.formulaid,
                    AlertId = ioan.alertegid,
                    ColourId = ioan.color
                };
            }
            else
            {
                sensor = new RefrigerationData.TemperatureSensor
                {
                    Id = ioan.id,
                    Name = ioan.name,
                    Temperature = new Temperature(ioan.value),
                    FormulaId = ioan.formulaid,
                    AlertId = ioan.alertegid,
                    ColourId = ioan.color
                };
            }

            return sensor;
        }
    }
}
