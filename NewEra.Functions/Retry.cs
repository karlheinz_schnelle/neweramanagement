﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace NewEraFreezerData
{
    public static class Retry
    {
        public static void Do(
            Action action,
            TimeSpan retryInterval,
            int maxAttemptCount = 1)
        {
            Do<object>(() =>
            {
                action();
                return null;
            }, null, retryInterval, maxAttemptCount);
        }

        public static T Do<T>(
            Func<T> action,
            TimeSpan retryInterval,
            int maxAttemptCount = 3)
        {
            return Do(action, null, retryInterval, maxAttemptCount);
        }

        public static T Do<T>(
            Func<T> action,
            Action correctiveActionAfterFail,
            TimeSpan retryInterval,
            int maxAttemptCount)
        {
            var exceptions = new List<Exception>();

            for (int attempted = 0; attempted < maxAttemptCount; attempted++)
            {
                try
                {
                    if (attempted > 0)
                    {
                        Thread.Sleep(retryInterval);
                    }
                    return action();
                }
                catch (Exception ex)
                {
                    exceptions.Add(ex);
                    try
                    {
                        correctiveActionAfterFail?.Invoke();
                    }
                    catch (Exception e)
                    {
                        exceptions.Add(e);
                    }
                }
            }
            throw new AggregateException(exceptions);
        }
    }
}
