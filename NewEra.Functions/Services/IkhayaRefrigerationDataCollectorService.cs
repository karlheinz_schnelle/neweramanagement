﻿using System;
using System.IO;
using System.Net;
using System.Text;
using NewEra.Common.Dtos;
using NewEraFreezerData.Dtos;
using NewEraFreezerData.Factories;
using Newtonsoft.Json;

namespace NewEraFreezerData.Services
{
    public class IkhayaRefrigerationDataCollectorService
        : IRefrigerationDataCollectorService
    {
        private const string IkhayaDataUrl = "https://www.myfreezeronline.com/dashupdate.ashx";
        private const string Username = "alberto%40newerachickens.co.za";
        private const string Password = "alberto";

        public RefrigerationData GetFreezerData()
        {
            var wc = new CookieAwareWebClient();

            string response = Retry.Do(() => wc.DownloadString(IkhayaDataUrl), () => wc.SetCookieContainer(LoginAndGetCookieContainer()), TimeSpan.FromSeconds(2), 2);

            RawIkhayaData rawData = JsonConvert.DeserializeObject<RawIkhayaData>(response);

            return FreezerDataFactory.Create(rawData);
        }

        private CookieContainer LoginAndGetCookieContainer()
        {
            string url = "https://www.myfreezeronline.com/";

            CookieContainer myCookieContainer = new CookieContainer();
            HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
            request.CookieContainer = myCookieContainer;
            request.Method = "GET";
            request.KeepAlive = false;

            HttpWebResponse response = request.GetResponse() as HttpWebResponse;

            Stream responseStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
            string srcString = reader.ReadToEnd();

            // get the page ViewState                
            string viewStateFlag = "id=\"__VIEWSTATE\" value=\"";
            int i = srcString.IndexOf(viewStateFlag) + viewStateFlag.Length;
            int j = srcString.IndexOf("\"", i);
            string viewState = srcString.Substring(i, j - i);

            // get page EventValidation                
            string eventValidationFlag = "id=\"__EVENTVALIDATION\" value=\"";
            i = srcString.IndexOf(eventValidationFlag) + eventValidationFlag.Length;
            j = srcString.IndexOf("\"", i);
            string eventValidation = srcString.Substring(i, j - i);

            string submitButton = "Log+In";

            // UserName and Password
            string userName = Username;
            string password = Password;
            // Convert the text into the url encoding string
            viewState = WebUtility.UrlEncode(viewState);
            eventValidation = WebUtility.UrlEncode(eventValidation);
            submitButton = WebUtility.UrlEncode(submitButton);

            // Concat the string data which will be submit
            string formatString =
                "__EVENTTARGET=&__EVENTARGUMENT=&__LASTFOCUS=&__VIEWSTATE={3}&__VIEWSTATEGENERATOR=CA0B0334&__VIEWSTATEENCRYPTED=&__EVENTVALIDATION={4}&pgc%24txtEmail={0}&pgc%24txtPassword={1}&pgc%24btnLogin={2}&pgc%24txtValue=&ajax%24chExceptionEventsSplitDateTime=on&ctl05=&ctl06=";
            string postString =
                     string.Format(formatString, userName, password, submitButton, viewState, eventValidation);

            // Convert the submit string data into the byte array
            byte[] postData = Encoding.ASCII.GetBytes(postString);

            // Set the request parameters
            request = WebRequest.Create(url) as HttpWebRequest;
            request.Method = "POST";
            request.Referer = url;
            request.KeepAlive = false;
            request.UserAgent = "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; InfoPath.2; CIBA)";
            request.ContentType = "application/x-www-form-urlencoded";
            request.CookieContainer = myCookieContainer;
            Cookie ck = new Cookie("TestCookie1", "Value of test cookie")
            {
                Domain = request.RequestUri.Host
            };
            request.CookieContainer.Add(ck);
            request.CookieContainer.Add(response.Cookies);

            request.ContentLength = postData.Length;

            // Submit the request data
            Stream outputStream = request.GetRequestStream();
            request.AllowAutoRedirect = true;
            outputStream.Write(postData, 0, postData.Length);
            outputStream.Close();

            // Get the return data
            request.GetResponse();

            return request.CookieContainer;
        }
    }
}
