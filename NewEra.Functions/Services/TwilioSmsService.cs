﻿using System.Collections.Generic;
using System.Configuration;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;

namespace NewEra.Functions.Services
{
    public class TwilioSmsService
    : SmsService
    {
        public override void SendSms(
            IList<string> mobileNumbers,
                string message)
        {
            string accountSid = ConfigurationManager.AppSettings["TwilioAccountSID"];
            string authToken = ConfigurationManager.AppSettings["TwilioAuthToken"];
            TwilioClient.Init(accountSid, authToken);

            foreach (var telephoneNo in mobileNumbers)
            {
                MessageResource.Create(to: new PhoneNumber(telephoneNo),
                    from: new PhoneNumber("+18582603709"),
                    body: message);
            }
        }
    }
}
