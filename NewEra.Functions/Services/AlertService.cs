﻿using System.Collections.Generic;
using System.Configuration;
using NewEra.Common.Enums;
using NewEra.Data.Models;

namespace NewEra.Functions.Services
{
    public class AlertService
    {
        private readonly IList<string> notificationMobileNumbers;

        public AlertService()
        {
            notificationMobileNumbers = ConfigurationManager.AppSettings["AlertNotificationMobileNumbers"].Split(',');
        }

        public void RaiseAlert(
            AlertType alertType,
            string alertEntityIdentifier)
        {
            if (Alert.OpenAlertExists(alertType.ToString(), alertEntityIdentifier))
            {
                return;
            }

            Alert.Create(alertType.ToString(), alertEntityIdentifier);

            new TwilioSmsService().SendSms(notificationMobileNumbers, $"New Era Chickens Alert: {alertType.ToString()} - {alertEntityIdentifier}");

            new EventService().RaiseEvent(EventType.AlertRaised);
        }

        public void CloseOpenAlert(
            AlertType alertType,
            string alertEntityIdentifier)
        {
            if (!Alert.OpenAlertExists(alertType.ToString(), alertEntityIdentifier))
            {
                return;
            }

            Alert.CloseOpenAlert(alertType.ToString(), alertEntityIdentifier);

            new TwilioSmsService().SendSms(notificationMobileNumbers, $"CLOSED - New Era Chickens Alert: {alertType.ToString()} - {alertEntityIdentifier}");

            new EventService().RaiseEvent(EventType.AlertRaised);
        }
    }
}
