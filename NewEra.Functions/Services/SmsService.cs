﻿using System.Collections.Generic;

namespace NewEra.Functions.Services
{
    public interface ISmsService
    {
        void SendSms(IList<string> mobileNumbers, string message);
    }

    public abstract class SmsService
    : ISmsService
    {
        public abstract void SendSms(IList<string> mobileNumbers, string message);
    }
}
