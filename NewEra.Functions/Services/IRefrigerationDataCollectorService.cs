﻿using NewEra.Common.Dtos;

namespace NewEraFreezerData.Services
{
    public interface IRefrigerationDataCollectorService
    {
        RefrigerationData GetFreezerData();
    }
}
