﻿namespace NewEra.Common.Enums
{
    public enum EventType
    {
        AlertRaised,
        AlertClosed
    }
}
