﻿namespace NewEra.Common.Enums
{
    public enum AlertType
    {
        FreezerOffline,
        VehicleOffline
    }
}
