﻿using System;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace NewEra.Common.ValueObjects
{
    [Serializable]
    public struct Temperature
        : ISerializable
    {
        private readonly int value;
        private readonly MeasurementUnit unit;

        public Temperature(
            string temperature)
        {
            unit = MeasurementUnit.Celsius;
            if (!int.TryParse(temperature.Substring(0, temperature.IndexOf(' ')), out value))
            {
                value = int.MinValue;
            }
        }

        public Temperature(
            SerializationInfo info,
            StreamingContext context)
        {
            if (info == null)
            {
                throw new ArgumentNullException(nameof(info));
            }

            value = (int)info.GetValue("value", typeof(int));
            unit = (MeasurementUnit)Enum.Parse(typeof(MeasurementUnit), (string)info.GetValue("unit", typeof(string)));
        }

        [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter)]
        public void GetObjectData(
            SerializationInfo info,
            StreamingContext context)
        {
            if (info == null)
            {
                throw new ArgumentNullException(nameof(info));
            }
            info.AddValue("value", value);
            info.AddValue("unit", unit.ToString());
        }

        public override string ToString()
        {
            if (!IsValid)
            {
                return "Invalid Temp";
            }
            return $"{value}°C";
        }

        public string Status
        {
            get
            {
                if (!IsValid)
                {
                    return "temp-invalid";
                }
                if (value <= -2)
                {
                    return "temp-low";
                }
                if (value <= 5)
                {
                    return "temp-medium";
                }
                return "temp-high";
            }
        }

        private bool IsValid => value != Int32.MinValue && value != Int32.MaxValue;

        private enum MeasurementUnit
        {
            Celsius,
            Fahrenheit,
            Kelvin
        }
    }
}
