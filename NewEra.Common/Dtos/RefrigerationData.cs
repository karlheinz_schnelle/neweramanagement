﻿using System.Collections.Generic;
using System.Linq;
using NewEra.Common.ValueObjects;

namespace NewEra.Common.Dtos
{
    public class RefrigerationData
    {
        public List<Freezer> Freezers { get; set; }
        public List<Vehicle> Vehicles { get; set; }

        public int MaxNumberOfFreezerSensors
        {
            get
            {
                return Freezers.Select(freezer => freezer.Sensors.Count).Concat(new[] { 0 }).Max();
            }
        }

        public int MaxNumberOfVehicleSensors
        {
            get
            {
                return Vehicles.Select(vehicle => vehicle.Sensors.Count).Concat(new[] { 0 }).Max();
            }
        }

        public RefrigerationData()
        {
            Freezers = new List<Freezer>();
            Vehicles = new List<Vehicle>();
        }

        public abstract class RefrigerationUnit
        {
            public int Id { get; set; }

            public string Name { get; set; }

            public bool Online { get; set; }

            public List<ISensor> Sensors { get; set; }

            public RefrigerationUnit()
            {
                Sensors = new List<ISensor>();
            }
        }

        public class Freezer
            : RefrigerationUnit
        {
            public string FreezerName => Name.Contains(" - ") ? Name.Substring(Name.LastIndexOf(" - ") + 3) : string.Empty;
        }

        public class Vehicle
            : RefrigerationUnit
        {
            public string NumberPlate
            {
                get
                {
                    string cleanedName = Name.Replace("â€“", "-").Replace(" - ", "-").Replace("-", " - ");

                    return cleanedName.Contains(' ')
                        ? cleanedName.Substring(cleanedName.LastIndexOf(' '))
                        : string.Empty;
                }
            }
        }

        public interface ISensor
        {
            int Id { get; set; }
            string Name { get; set; }
            int FormulaId { get; set; }
            int AlertId { get; set; }
            int ColourId { get; set; }
        }

        public abstract class Sensor
            : ISensor
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public int FormulaId { get; set; }
            public int AlertId { get; set; }
            public int ColourId { get; set; }
        }

        public class TemperatureSensor
            : Sensor
        {
            public Temperature Temperature { get; set; }
        }

        public class PowerSensor
            : Sensor
        {
            public bool IsOn { get; set; }
        }

        public class IOSensor
            : Sensor
        {
            public bool IsOn { get; set; }
        }
    }
}
