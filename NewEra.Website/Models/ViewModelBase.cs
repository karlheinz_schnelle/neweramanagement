﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewEraManagement.Models
{
    public abstract class ViewModelBase
    {
        public string BackUrl { get; set; }
    }
}