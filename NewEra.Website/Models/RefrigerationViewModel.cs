﻿using System;
using NewEra.Common.Dtos;

namespace NewEraManagement.Models
{
    public class RefrigerationViewModel
        : ViewModelBase
    {
        public DateTime TimeStamp { get; set; }
        public RefrigerationData RefrigerationData { get; set; }
    }
}