﻿namespace NewEraManagement.Models
{
    public class WidgetModel
    {
        public string Url { get; set; }
        public int RefreshFrequency { get; set; }
        public string DetailUrl { get; set; }

        public WidgetModel()
        {
        }

        public WidgetModel(
            string url, 
            int refeshFrequency, 
            string detailUrl = null)
        {
            Url = url;
            RefreshFrequency = refeshFrequency;
            DetailUrl = detailUrl;
        }
    }
}