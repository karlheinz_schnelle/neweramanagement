﻿namespace NewEraManagement.Models
{
    public class TileModel
    {
        public string Style { get; set; }
        public string Title { get; set; }
        public string Contents { get; set; }
        public string Url { get; set; }
        public string LinkType { get; set; }

        public TileModel(
            string style,
            string title,
            string contents,
            string url = null,
            string linkType = "NewPage")
        {
            Style = style;
            Title = title;
            Contents = contents;
            Url = url;
            LinkType = linkType;
        }
    }
}