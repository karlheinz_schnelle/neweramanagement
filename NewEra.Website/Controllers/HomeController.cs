﻿using System.Web.Mvc;

namespace NewEraManagement.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult FoundersView()
        {
            return RedirectToAction("Index", "FoundersView");
        }
    }
}