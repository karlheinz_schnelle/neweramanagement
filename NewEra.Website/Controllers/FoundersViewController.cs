﻿using System.Web.Mvc;
using NewEra.Common.Dtos;
using NewEra.Data;
using NewEraManagement.Models;
using Newtonsoft.Json;

namespace NewEraManagement.Controllers
{
    public class FoundersViewController
        : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Refrigeration()
        {
            var refrigerationData = new RefrigerationDataRepository().GetLatestRefrigerationData("FoundersView");

            RefrigerationViewModel model =
                new RefrigerationViewModel
                {
                    TimeStamp = refrigerationData.Timestamp.DateTime.AddHours(2),
                    RefrigerationData =
                        JsonConvert.DeserializeObject<RefrigerationData>(refrigerationData.RefrigerationData, new JsonSerializerSettings
                        {
                            TypeNameHandling = TypeNameHandling.All
                        }),
                    BackUrl = Url.Action("Index")
                };

            return View("_Refrigeration", model);
        }
    }
}