﻿var Dashboard = function () {
	var registerWidget = function () {
		var element = $(this);

		var url = element.data("widget-url");
		var detailUrl = element.data("widget-detail-url");
		var frequency = element.data("widget-refresh-frequency");

		var refreshWidget = function () {
			element.load(url,
				function () {
					element.find("[data-detail]").click(function () {
						$('#detailModal .modal-body').text("Loading...");
						$('#detailModal .modal-body').load(detailUrl);
						$('#detailModal').modal('show');
					});
				});
		};

		refreshWidget();

		window.setInterval(refreshWidget, frequency || 10000);
	};

	var registerWidgets = function () {
		$("[data-widget]").each(registerWidget);
	};

	var registerTile = function () {
		var element = $(this);

		var linkUrl = element.data("link-url");

		element.find("a.NewPage").click(function ()
		{
			window.location = linkUrl;
		});

		element.find("a.Popup").click(function ()
		{
			$('#detailModal .modal-body').text("Loading...");
			$('#detailModal .modal-body').load(linkUrl);
			$('#detailModal').modal('show');
		});
	}

	var registerTiles = function () {
		$("[data-tile]").each(registerTile);
	};

	return {
		RegisterWidgets: registerWidgets,
		RegisterTiles: registerTiles
	};
}();

$(document).ready(function () {
	Dashboard.RegisterWidgets();
	Dashboard.RegisterTiles();
});