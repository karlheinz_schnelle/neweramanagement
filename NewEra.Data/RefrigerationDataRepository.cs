﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Table;
using NewEra.Common.Dtos;
using Newtonsoft.Json;

namespace NewEra.Data
{
    public interface IRefrigerationDataRepository
    {
        void SaveRefrigerationData(RefrigerationData data);
        RefrigerationDataEntity GetLatestRefrigerationData(string locationName);
    }

    public class RefrigerationDataRepository
        : IRefrigerationDataRepository
    {
        public void SaveRefrigerationData(
            RefrigerationData data)
        {
            CloudStorageAccount storageAccount = new CloudStorageAccount(
                new StorageCredentials("newerarefrigerationdata",
                    "fhmX6OKzwFRoG/iWK5X9o7q0ugUGoMOzQmgMhFFjz5ubFL/rrUNFCaZP/gwEhzh6fzirACIo/HugyvrSNEBjcA=="), true);

            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();

            CloudTable table = tableClient.GetTableReference("RefrigerationData");

            TableOperation insertOperation =
                TableOperation.InsertOrMerge(new RefrigerationDataEntity("FoundersView",
                    JsonConvert.SerializeObject(data, new JsonSerializerSettings
                    {
                        TypeNameHandling = TypeNameHandling.All
                    })));

            table.Execute(insertOperation);
        }

        public RefrigerationDataEntity GetLatestRefrigerationData(
            string locationName)
        {
            CloudStorageAccount storageAccount = new CloudStorageAccount(
                new StorageCredentials("newerarefrigerationdata",
                    "fhmX6OKzwFRoG/iWK5X9o7q0ugUGoMOzQmgMhFFjz5ubFL/rrUNFCaZP/gwEhzh6fzirACIo/HugyvrSNEBjcA=="), true);

            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();

            CloudTable table = tableClient.GetTableReference("RefrigerationData");

            TableOperation operation = TableOperation.Retrieve<RefrigerationDataEntity>(locationName, locationName);

            return (RefrigerationDataEntity)table.Execute(operation).Result;
        }
    }
}
