//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NewEra.Data.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Alert
    {
        public System.Guid AlertId { get; set; }
        public string AlertType { get; set; }
        public string AlertEntityIdentifier { get; set; }
        public System.DateTime AlertOpenDate { get; set; }
        public Nullable<System.DateTime> AlertClosedDate { get; set; }
    }
}
