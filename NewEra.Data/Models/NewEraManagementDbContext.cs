﻿using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Core.EntityClient;

namespace NewEra.Data.Models
{
    [DbConfigurationType(typeof(NewEraManagementDbConfiguration))]
    public partial class NewEraManagementDbContext
    {
        public static NewEraManagementDbContext GetDbContext()
        {
            var connString = ConfigurationManager.ConnectionStrings["NewEraManagement"];
            EntityConnectionStringBuilder b = new EntityConnectionStringBuilder
            {
                Metadata = "res://*/Models.NewEraManagement.csdl|res://*/Models.NewEraManagement.ssdl|res://*/Models.NewEraManagement.msl",
                ProviderConnectionString = connString.ConnectionString,
                Provider = "System.Data.SqlClient"
            };
            return new NewEraManagementDbContext(b.ConnectionString);
        }

        private NewEraManagementDbContext(
            string connectionString) : base(connectionString)
        {
        }
    }
}
