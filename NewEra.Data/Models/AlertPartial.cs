﻿using System;
using System.Linq;

namespace NewEra.Data.Models
{
    public partial class Alert
    {
        public static Alert Create(
            string alertType,
            string alertEntityIdentifier)
        {
            Alert alert = new Alert
            {
                AlertId = Guid.NewGuid(),
                AlertType = alertType,
                AlertEntityIdentifier = alertEntityIdentifier,
                AlertOpenDate = DateTime.Now
            };

            using (var db = NewEraManagementDbContext.GetDbContext())
            {
                db.Alerts.Add(alert);
                db.SaveChanges();
            }

            return alert;
        }

        public static void CloseOpenAlert(
            string alertType,
            string alertEntityIdentifier)
        {
            using (var db = NewEraManagementDbContext.GetDbContext())
            {
                var query = from a in db.Alerts
                            where a.AlertType == alertType
                                  && a.AlertEntityIdentifier == alertEntityIdentifier
                                  && a.AlertClosedDate == null
                            select a;

                var openAlert = query.SingleOrDefault();

                if (openAlert != null)
                {
                    openAlert.AlertClosedDate = DateTime.Now;
                    db.SaveChanges();
                }
            }
        }

        public static bool OpenAlertExists(
            string alertType,
            string alertEntityIdentifier)
        {
            using (var db = NewEraManagementDbContext.GetDbContext())
            {
                var query = from a in db.Alerts
                            where a.AlertType == alertType
                                  && a.AlertEntityIdentifier == alertEntityIdentifier
                                  && a.AlertClosedDate == null
                            select a;

                return query.Any();
            }
        }
    }
}
