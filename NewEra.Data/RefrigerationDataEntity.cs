﻿using Microsoft.WindowsAzure.Storage.Table;

namespace NewEra.Data
{
    public class RefrigerationDataEntity
        : TableEntity
    {
        public string RefrigerationData { get; set; }

        public RefrigerationDataEntity()
        {
        }

        public RefrigerationDataEntity(
            string locationName,
            string refrigerationData)
        {
            PartitionKey = locationName;
            RowKey = locationName;
            RefrigerationData = refrigerationData;
        }
    }
}
